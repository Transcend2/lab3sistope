# MAKEFILE
# Lab3 SISTOPE
# Daniel Gacitúa e Ian Orellana

SOURCES = main
DSOURCES = main-dbg
CFLAGS = -std=c99 -Wall
DFLAGS = -std=c99 -Wall -g

SDIR = ./src
BDIR = ./build

all: release

release: $(SOURCES)

debug: $(DSOURCES)

main:
	@echo -e '\e[0;32m=> \e[1;31mCompilando objetivo $@\e[m'
	mkdir -p $(BDIR)
	gcc $(SDIR)/main.c -o $(BDIR)/mainProgram $(CFLAGS)

main-dbg:
	@echo -e '\e[0;32m=> \e[1;31mCompilando objetivo $@\e[m'
	mkdir -p $(BDIR)
	gcc $(SDIR)/main.c -o $(BDIR)/mainProgram $(DFLAGS)

.PHONY: clean

clean:
	rm -rf $(BDIR)/*