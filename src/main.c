#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <stdio.h>
#include <string.h>

#include "libmem.h"
#include "libalg.h"

#define TRUE 1
#define FALSE 0

#define MEMTEST 64
#define RUTA 256

int main(int argc, char **argv){
  ////////////Variables Getopt//////////
  int c, contador=0;
  char *flag1=NULL, *flag2=NULL, *flag3=NULL, *flag4=NULL,*flag5=NULL;
  ////////Proceso Getopt///////////////////
  while (1)
    {
      static struct option long_options[] =
        {
          
          {"peticiones",required_argument, 0, 'p'},
          {"algoritmo",required_argument,0,'a' },
          {"cantidadmemoria",required_argument,0,'c'},
          {"salida",required_argument,0,'s'},
          {"modo",required_argument,0,'m'},
          {0, 0, 0, 0}
        };
      
      int option_index = 0;

      c = getopt_long (argc, argv, "p:a:c:s:m:",long_options, &option_index);
      if (c == -1)
        break;

      switch (c)
        {
        case 'p':
          flag1=optarg;
          contador++;
          break;
        case 'a':
          flag2=optarg;
          contador++;
          break;
        case 'c':
          flag3=optarg;
          contador++;
          break;
        case 's':
          flag4=optarg;
          contador++;
          break;
        case 'm':
          flag5=optarg;
          contador++;
          break;

        default:
          abort ();
          return 1;
        }
    }
  //////////////End of Getopt/////////////////////////
    

    if(contador==4 || contador==5){
      if(flag1==NULL || flag2==NULL || flag3==NULL || flag5==NULL){
        printf("Error, falto un obligatorio\n");
      }else{
        EspacioMem em = crearMemoria(atoi(flag3));
        if(flag4==NULL){
          FILE *archivo=fopen("salida.txt","w");
          fclose(archivo);
          LeeArchivos(em,flag1,cuentaLineas(flag1),flag5,"salida.txt",flag2);
        }else{
          FILE *archivo=fopen(flag4,"w");
          fclose(archivo);
          LeeArchivos(em,flag1,cuentaLineas(flag1),flag5,flag4,flag2);
        }
      }
    }else if(contador<4){
      printf("Error: Pocos argumentos.\nLa entrada requerida debe presentar uno de los siguientes formatos:\n");
      printf("1.-\n--peticiones <Nombre archivo> --algoritmo <B o F o N> --cantidadmemoria <Cantidad de Megas>");
      printf(" --salida <Nombre archivo(opcional)> --modo <F o N o B>\n");
      printf("2.-\n-p <Nombre archivo> -a <B o F o N> -c <Cantidad de Megas -s <Nombre archivo(opcional)> -m <F o N o B>\n>");
    }else if(contador>5){
      printf("Error: Demasiados argumentos.\nLa entrada requerida debe presentar uno de los siguientes formatos:\n");
      printf("1.-\n--peticiones <Nombre archivo> --algoritmo <B o F o N> --cantidadmemoria <Cantidad de Megas>");
      printf(" --salida <Nombre archivo(opcional)> --modo <F o N o B>\n");
      printf("2.-\n-p <Nombre archivo> -a <B o F o N> -c <Cantidad de Megas -s <Nombre archivo(opcional)> -m <F o N o B>\n>");
    }


  return 0;
}
