/* Biblioteca para los algoritmos de posicionamiento */


#define NOMBRE_ARCHIVO 128
#define NOMBRE_PETICION 30
#define LARGO_LINEA 256

#define MEMTEST 64

#define	PROCESO_ELIMINADO 2
#define PROCESO_AGREGADO 1
#define PROCESO_PENDIENTE 0
#define PROCESO_ERROR -1
#define	PROCESO_DEFRAG -2
#define PROCESO_SINMEMORIA -3
#define ARRIBA 1
#define ABAJO 0

#define BESTFIT_ERROR -2
#define BESTFIT_NULL -1
#define BESTFIT_DEFAULT 0
#define BESTFIT_INICIO 1
#define BESTFIT_MEDIO 2
#define BESTFIT_FIN 3
#define BESTFIT_DEFRAG 4

void desFragmentar(EspacioMem *em, char *modo, char *salida) {
	BloqueMem *aux, *ant;
	size_t mem;
	aux = em->inicio;
	ant = NULL;
	if(aux){
		if(strcmp(modo,"H")==0){
			printf("Desfragmenta\n");
		}else if(strcmp(modo,"S")==0){
			FILE *archivo=fopen(salida,"a");
			fprintf(archivo,"Desfragmenta\n");
			fclose(archivo);
		}
			if (aux->dirInicio!=0) {
				mem=aux->dirInicio;
				aux->dirFin=aux->dirFin-mem;
				aux->dirInicio=0;
				if (aux->sig) {
					ant=aux;
					aux=aux->sig;	
				}			
			}else{
				if (aux->sig) {
					ant=aux;
					aux=aux->sig;
				}
			}
			
				while(aux->sig!=NULL){
					if((aux->dirInicio-ant->dirFin)==1){
						if (aux->sig) {
							ant=aux;
							aux=aux->sig;
						}
					}else{//printf("%s\n",aux->nombreProceso );
						mem=aux->dirInicio-ant->dirFin;
						aux->dirInicio=ant->dirFin+1;
						aux->dirFin=aux->dirFin-mem+1;
						if (aux->sig) {
							ant=aux;
							aux=aux->sig;	
						}
					}
				}
			
				mem=aux->dirInicio-ant->dirFin;
				aux->dirInicio=ant->dirFin+1;
				aux->dirFin=aux->dirFin-mem+1;
	}
	(em->cantDefrags)++;
}

void Historial(char *nombre, size_t tamano, size_t inicio, size_t fin,char *modo, char* salida){
	if(strcmp(modo,"H")==0){
		printf("Se inserta proceso: %s, de tamaño %zu en el espacio %zu-%zu\n",nombre,tamano,inicio,fin);
	}else if(strcmp(modo,"S")==0){
		FILE *archivo=fopen(salida,"a");
		fprintf(archivo,"Se inserta proceso: %s, de tamaño %zu en el espacio %zu-%zu\n",nombre,tamano,inicio,fin);
		fclose(archivo);
	}
}

void agregarFirstFit(EspacioMem *em, BloqueMem *proc, char *modo, char *salida) {
	size_t bloqueInicio, bloqueFin;
	int flag = ABAJO;
	int insertado = FALSE;
	BloqueMem *aux = (BloqueMem*)malloc(sizeof(BloqueMem));

	aux = em->inicio;

	while (insertado == FALSE) {
		if (aux == NULL) {		// Toda la memoria libre
			if (proc->tamano <= em->tamanoTotal) {
				em->inicio = proc;

				proc->dirInicio = 0;
				proc->dirFin = proc->tamano - 1;

				em->espacioOcupado += proc->tamano;
				em->espacioLibre -= proc->tamano;
				em->cursor = proc;

				insertado = TRUE;
				flag=ABAJO;
				Historial(proc->nombreProceso,proc->tamano,proc->dirInicio,proc->dirFin,modo,salida);
			}
			else {
				if(flag==ABAJO){
					desFragmentar(em,modo,salida);
					//agregarFirstFit(em, proc, modo, salida);
					flag=ARRIBA;
				}else{
					if(strcmp(modo,"H")==0){
						imprimeErrorHablador(proc->nombreProceso,proc->tamano);
					}else if(strcmp(modo,"S")==0){
						imprimeErrorSilencioso(proc->nombreProceso,proc->tamano,salida);

					}
					//printf("ERROR: Memoria libre insuficiente!\n");
					insertado = TRUE;
					flag=ABAJO;
				}
			}			
		}
		else {		// existen procesos
			if (aux->sig) { // existe un proceso siguiente
				bloqueInicio = aux->dirFin;
				bloqueFin = aux->sig->dirInicio;
			}
			else {	// ultimo proceso
				bloqueInicio = aux->dirFin;
				bloqueFin = em->tamanoTotal;
			}

			if (aux == em->inicio) {	// aux es el primer proceso
				if (proc->tamano <= aux->dirInicio) {	// proceso cabe antes del primer proceso
					proc->sig = aux;
					em->inicio = proc;
					
					proc->dirInicio = 0;
					proc->dirFin = proc->tamano - 1;

					em->espacioOcupado += proc->tamano;
					em->espacioLibre -= proc->tamano;
					em->cursor = proc;

					insertado = TRUE;
					flag=ABAJO;
					Historial(proc->nombreProceso,proc->tamano,proc->dirInicio,proc->dirFin,modo,salida);
				}
				else if (proc->tamano <= (bloqueFin - bloqueInicio)) {		// proceso cabe despues del primer proceso
					proc->sig = aux->sig;
					aux->sig = proc;
					
					proc->dirInicio = bloqueInicio + 1;
					proc->dirFin = proc->dirInicio + proc->tamano - 1;

					em->espacioOcupado += proc->tamano;
					em->espacioLibre -= proc->tamano;
					em->cursor = proc;

					insertado = TRUE;
					flag=ABAJO;
					Historial(proc->nombreProceso,proc->tamano,proc->dirInicio,proc->dirFin,modo,salida);
				}
				else {		// proceso no cabe antes o después del primer proceso
					if (aux->sig) {
						aux = aux->sig;	
					}
					else {
						if(flag==ABAJO){
							desFragmentar(em,modo,salida);
							//agregarFirstFit(em, proc, modo, salida);
							flag=ARRIBA;
						}else{
							if(strcmp(modo,"H")==0){
								imprimeErrorHablador(proc->nombreProceso,proc->tamano);
							}else if(strcmp(modo,"S")==0){
								imprimeErrorSilencioso(proc->nombreProceso,proc->tamano,salida);

							}
							//printf("ERROR: Memoria libre insuficiente!\n");
							insertado = TRUE;
							flag=ABAJO;
						}
					}
				}
			}
			else {	// aux no es el primer proceso
				if (aux->sig) {	// aux no es el ultimo proceso
					if (proc->tamano <= (bloqueFin - bloqueInicio)) {
						proc->sig = aux->sig;
						aux->sig = proc;
						
						proc->dirInicio = bloqueInicio + 1;
						proc->dirFin = proc->dirInicio + proc->tamano - 1;

						em->espacioOcupado += proc->tamano;
						em->espacioLibre -= proc->tamano;
						em->cursor = proc;

						insertado = TRUE;
						flag=ABAJO;
						Historial(proc->nombreProceso,proc->tamano,proc->dirInicio,proc->dirFin,modo,salida);
					}
					else {
						if (aux->sig) {
							aux = aux->sig;	
						}
						else {
							if(flag==ABAJO){
								desFragmentar(em,modo,salida);
								//agregarFirstFit(em, proc, modo, salida);
								flag=ARRIBA;
							}else{
								if(strcmp(modo,"H")==0){
									imprimeErrorHablador(proc->nombreProceso,proc->tamano);
								}else if(strcmp(modo,"S")==0){
									imprimeErrorSilencioso(proc->nombreProceso,proc->tamano,salida);

								}
								//printf("ERROR: Memoria libre insuficiente!\n");
								insertado = TRUE;
								flag=ABAJO;
							}
						}
					}
				}
				else {		// aux es el ultimo proceso
					if (proc->tamano <= (bloqueFin - bloqueInicio)) {
						proc->sig = NULL;
						aux->sig = proc;

						proc->dirInicio = bloqueInicio + 1;
						proc->dirFin = proc->dirInicio + proc->tamano -1;

						em->espacioOcupado += proc->tamano;
						em->espacioLibre -= proc->tamano;
						em->cursor = proc;

						insertado = TRUE;
						flag=ABAJO;
						Historial(proc->nombreProceso,proc->tamano,proc->dirInicio,proc->dirFin,modo,salida);
					}
					else {
						if(flag==ABAJO){
							desFragmentar(em,modo,salida);
							//agregarFirstFit(em, proc, modo, salida);
							flag=ARRIBA;
						}else{
							if(strcmp(modo,"H")==0){
								imprimeErrorHablador(proc->nombreProceso,proc->tamano);
							}else if(strcmp(modo,"S")==0){
								imprimeErrorSilencioso(proc->nombreProceso,proc->tamano,salida);

							}
							//printf("ERROR: Memoria libre insuficiente!\n");
							insertado = TRUE;
							flag=ABAJO;
							//return PROCESO_SINMEMORIA;
						}
					}
				}
			}
		}
	}	
}

void agregarNextFit(EspacioMem *em, BloqueMem *proc, char *modo, char *salida) {
	size_t bloqueInicio, bloqueFin;
	int flag = ABAJO, insertado = FALSE, vuelta = FALSE;
	BloqueMem *aux = (BloqueMem*)malloc(sizeof(BloqueMem));

	aux = em->cursor;

	if (aux == em->inicio) {
		agregarFirstFit(em, proc, modo, salida);
	}
	else {
		while (insertado == FALSE) {
			if (aux == NULL) {		// Toda la memoria libre
				if (proc->tamano <= em->tamanoTotal) {
					em->inicio = proc;

					proc->dirInicio = 0;
					proc->dirFin = proc->tamano - 1;

					em->espacioOcupado += proc->tamano;
					em->espacioLibre -= proc->tamano;
					em->cursor = proc;

					insertado = TRUE;
					flag=ABAJO;
					//return PROCESO_AGREGADO;
				}
				else {
					if(flag==ABAJO){
						desFragmentar(em,modo,salida);
						//agregarFirstFit(em, proc, modo, salida);
						flag=ARRIBA;
						em->cursor=proc;
					}else{printf("1\n");
						if(strcmp(modo,"H")==0){
							imprimeErrorHablador(proc->nombreProceso,proc->tamano);
						}else if(strcmp(modo,"S")==0){
							imprimeErrorSilencioso(proc->nombreProceso,proc->tamano,salida);

						}
						//printf("ERROR: Memoria libre insuficiente!\n");
						insertado = TRUE;
						flag=ABAJO;
						//return PROCESO_SINMEMORIA;
					}
				}			
			}
			else {		// existen procesos
				if (aux->sig) { // existe un proceso siguiente
					bloqueInicio = aux->dirFin;
					bloqueFin = aux->sig->dirInicio;
				}
				else {	// ultimo proceso
					bloqueInicio = aux->dirFin;
					bloqueFin = em->tamanoTotal;
				}

				if (aux == em->inicio) {	// aux es el primer proceso
					if (proc->tamano <= aux->dirInicio) {	// proceso cabe antes del primer proceso
						proc->sig = aux;
						em->inicio = proc;
						
						proc->dirInicio = 0;
						proc->dirFin = proc->tamano - 1;

						em->espacioOcupado += proc->tamano;
						em->espacioLibre -= proc->tamano;
						em->cursor = proc;

						insertado = TRUE;
						flag=ABAJO;
						Historial(proc->nombreProceso,proc->tamano,proc->dirInicio,proc->dirFin,modo,salida);
					}
					else if (proc->tamano <= (bloqueFin - bloqueInicio)) {		// proceso cabe despues del primer proceso
						proc->sig = aux->sig;
						aux->sig = proc;
						
						proc->dirInicio = bloqueInicio + 1;
						proc->dirFin = proc->dirInicio + proc->tamano - 1;

						em->espacioOcupado += proc->tamano;
						em->espacioLibre -= proc->tamano;
						em->cursor = proc;

						insertado = TRUE;
						flag=ABAJO;
						Historial(proc->nombreProceso,proc->tamano,proc->dirInicio,proc->dirFin,modo,salida);
					}
					else {		// proceso no cabe antes o después del primer proceso
						if (aux->sig && (aux->sig != em->cursor)) {
							aux = aux->sig;	
						}
						else {
							if(flag==ABAJO){
								desFragmentar(em,modo,salida);
								//agregarFirstFit(em, proc, modo, salida);
								flag=ARRIBA;
								em->cursor=proc;
							}else{
								if(strcmp(modo,"H")==0){printf("2\n");
									imprimeErrorHablador(proc->nombreProceso,proc->tamano);
								}else if(strcmp(modo,"S")==0){
									imprimeErrorSilencioso(proc->nombreProceso,proc->tamano,salida);

								}
								//printf("ERROR: Memoria libre insuficiente!\n");
								insertado = TRUE;
								flag=ABAJO;
								//return PROCESO_SINMEMORIA;
							}
						}
					}
				}
				else {	// aux no es el primer proceso
					if (aux->sig) {	// aux no es el ultimo proceso
						if (proc->tamano <= (bloqueFin - bloqueInicio)) {
							proc->sig = aux->sig;
							aux->sig = proc;
							
							proc->dirInicio = bloqueInicio + 1;
							proc->dirFin = proc->dirInicio + proc->tamano - 1;

							em->espacioOcupado += proc->tamano;
							em->espacioLibre -= proc->tamano;
							em->cursor = proc;

							insertado = TRUE;
							flag=ABAJO;
							Historial(proc->nombreProceso,proc->tamano,proc->dirInicio,proc->dirFin,modo,salida);
						}
						else {
							if (aux->sig && (aux->sig != em->cursor)) {
								aux = aux->sig;
							}
							else {
								if(flag==ABAJO){
									desFragmentar(em,modo,salida);
									//agregarFirstFit(em, proc, modo, salida);
									em->cursor=proc;
									flag=ARRIBA;
								}else{
									if(strcmp(modo,"H")==0){printf("3\n");
										imprimeErrorHablador(proc->nombreProceso,proc->tamano);
									}else if(strcmp(modo,"S")==0){
										imprimeErrorSilencioso(proc->nombreProceso,proc->tamano,salida);

									}
									//printf("ERROR: Memoria libre insuficiente!\n");
									insertado = TRUE;
									flag=ABAJO;
									//return PROCESO_SINMEMORIA;
								}
							}
						}
					}
					else {		// aux es el ultimo proceso
						if (proc->tamano <= (bloqueFin - bloqueInicio)) {
							proc->sig = NULL;
							aux->sig = proc;

							proc->dirInicio = bloqueInicio + 1;
							proc->dirFin = proc->dirInicio + proc->tamano -1;

							em->espacioOcupado += proc->tamano;
							em->espacioLibre -= proc->tamano;
							em->cursor = proc;

							insertado = TRUE;
							flag=ABAJO;
							Historial(proc->nombreProceso,proc->tamano,proc->dirInicio,proc->dirFin,modo,salida);
						}
						else {
							if (em->inicio && vuelta==FALSE) {
								vuelta = TRUE;
								aux = em->inicio;
							}
							else {
								if(flag==ABAJO){
									desFragmentar(em,modo,salida);
									//agregarFirstFit(em, proc, modo, salida);
									em->cursor=proc;
									flag=ARRIBA;
								}else{
									if(strcmp(modo,"H")==0){printf("4\n");
										imprimeErrorHablador(proc->nombreProceso,proc->tamano);
									}else if(strcmp(modo,"S")==0){
										imprimeErrorSilencioso(proc->nombreProceso,proc->tamano,salida);

									}
									insertado = TRUE;
									flag=ABAJO;
									//return PROCESO_SINMEMORIA;
								}
							}							
						}
					}
				}
			}
		}
	}	
}

void agregarBestFit(EspacioMem *em, BloqueMem *proc, char *modo, char *salida) {
	size_t bloqueInicio, bloqueFin, delta, bloqueMax, refInicio, refFin, acum;
	int insertable = BESTFIT_DEFAULT, flagDefrag = ABAJO;
	BloqueMem *aux = (BloqueMem*)malloc(sizeof(BloqueMem));
	BloqueMem *ref = (BloqueMem*)malloc(sizeof(BloqueMem));

	bloqueInicio = bloqueFin = refInicio = refFin = delta = acum = 0;
	bloqueMax = em->tamanoTotal;

	if (!(em->inicio)) {		// memoria vacia
		if (proc->tamano <= em->tamanoTotal) {
			em->inicio = proc; // agregar primer proceso
			proc->sig = NULL;

			proc->dirInicio = 0;
			proc->dirFin = proc->tamano - 1;

			em->espacioOcupado += proc->tamano;
			em->espacioLibre -= proc->tamano;
			em->cursor = proc;

			insertable = BESTFIT_NULL;
		}
		else {
			insertable = BESTFIT_ERROR;
		}		
	}
	else {		// memoria con procesos
		aux = em->inicio;
		while (aux) {
			if (aux->sig) { // existe un proceso siguiente
				bloqueInicio = aux->dirFin;
				bloqueFin = aux->sig->dirInicio;
				delta = bloqueFin - bloqueInicio;
			}
			else {	// ultimo proceso
				bloqueInicio = aux->dirFin;
				bloqueFin = em->tamanoTotal;
				delta = bloqueFin - bloqueInicio;
			}

			if (aux == em->inicio) {	// aux es el primer proceso
				if ((proc->tamano <= aux->dirInicio) && (bloqueMax > aux->dirInicio)) {	// proceso entra al antes
					refInicio = 0;
					refFin = bloqueFin - 1;
					bloqueMax = refFin - refInicio;
					ref = aux;
					flagDefrag = ARRIBA;
					insertable = BESTFIT_INICIO;
				}
				else if ((proc->tamano <= delta) && (bloqueMax > delta)) {		// proceso entra despues
					if (aux->sig) {
						refInicio = bloqueInicio + 1;
						refFin = bloqueFin - 1;
						bloqueMax = refFin - refInicio;
						ref = aux;
						flagDefrag = ARRIBA;
						insertable = BESTFIT_MEDIO;	
					}
					else {
						refInicio = bloqueInicio + 1;
						refFin = bloqueFin - 1;
						bloqueMax = refFin - refInicio;
						ref = aux;
						flagDefrag = ARRIBA;
						insertable = BESTFIT_FIN;
					}					
				}
				else {		// proceso no entra
					acum += delta;
					if (!(aux->sig)) {
						if (flagDefrag==ABAJO) {
							if (acum >= proc->tamano) {
								insertable = BESTFIT_DEFRAG;
							}
							else {
								insertable = BESTFIT_ERROR;	
							}
						}
					}
				}
			}
			else {		// aux no es el primer proceso
				if ((proc->tamano <= delta) && (bloqueMax > delta)) {
					if (aux->sig) {
						refInicio = bloqueInicio + 1;
						refFin = bloqueFin - 1;
						bloqueMax = refFin - refInicio;
						ref = aux;
						flagDefrag = ARRIBA;
						insertable = BESTFIT_MEDIO;	
					}
					else {
						refInicio = bloqueInicio + 1;
						refFin = bloqueFin - 1;
						bloqueMax = refFin - refInicio;
						ref = aux;
						flagDefrag = ARRIBA;
						insertable = BESTFIT_FIN;
					}
				}
				else {	// proceso no entra
					acum += delta;
					if (!(aux->sig)) {
						if (flagDefrag==ABAJO) {
							if (acum >= proc->tamano) {
								insertable = BESTFIT_DEFRAG;
							}
							else {
								insertable = BESTFIT_ERROR;	
							}							
						}
					}
				}
			}
			aux = aux->sig;
		}

		if (insertable == BESTFIT_INICIO) {
			proc->sig = em->inicio;
			em->inicio = proc;

			proc->dirInicio = refInicio;
			proc->dirFin = refInicio + proc->tamano - 1;

			em->espacioOcupado += proc->tamano;
			em->espacioLibre -= proc->tamano;
			em->cursor = proc;
		}

		if (insertable == BESTFIT_MEDIO) {
			proc->sig = ref->sig;
			ref->sig = proc;
			
			proc->dirInicio = refInicio;
			proc->dirFin = refInicio + proc->tamano - 1;
			
			em->espacioOcupado += proc->tamano;
			em->espacioLibre -= proc->tamano;
			em->cursor = proc;
		}

		if (insertable == BESTFIT_FIN) {
			proc->sig = NULL;
			ref->sig = proc;
			
			proc->dirInicio = refInicio;
			proc->dirFin = refInicio + proc->tamano - 1;
			
			em->espacioOcupado += proc->tamano;
			em->espacioLibre -= proc->tamano;
			em->cursor = proc;
		}

		if (insertable == BESTFIT_DEFRAG) {
			desFragmentar(em,modo,salida);
			agregarBestFit(em, proc, modo, salida);
		}
		
		if (insertable == BESTFIT_ERROR) {
			if(strcmp(modo,"H")==0){
				imprimeErrorHablador(proc->nombreProceso,proc->tamano);
			}else if(strcmp(modo,"S")==0){
				imprimeErrorSilencioso(proc->nombreProceso,proc->tamano,salida);
			}
		}
	}
}


void eliminarProceso(EspacioMem *em, size_t direccion) {
	BloqueMem *aux, *ant;
	int estado;

	aux = em->inicio;
	ant = NULL;
	estado = PROCESO_PENDIENTE;
	if (!aux) {
		printf("ERROR: El espacio de memoria no tiene procesos\n"); // TODO fixear de acuerdo al log
	}
	else {
		while (estado==PROCESO_PENDIENTE) {
			if (direccion == aux->dirInicio) {		// la direccion inicial del proceso coincide con la direccion a borrar
				if (aux == em->inicio) {	// proceso a eliminar es el primero
					em->inicio = aux->sig;
					
					em->espacioOcupado -= aux->tamano;
					em->espacioLibre += aux->tamano;
				}
				else {	//proceso a eliminar no es el primero
					ant->sig = aux->sig;

					em->espacioOcupado -= aux->tamano;
					em->espacioLibre += aux->tamano;
				}

				estado = PROCESO_ELIMINADO;
				//return estado;
			}
			else {		// la direccion no es la del proceso
				if (aux->sig) {		// existe un proceso siguiente
					ant = aux;
					aux = aux->sig;
				}
				else {
					printf("ERROR: Solicitud de eliminar proceso en direccion %zu equivocada\n", direccion);
					estado = PROCESO_ERROR;
					//return estado;
				}
			}
		}	
	}	
	//return PROCESO_ERROR;
}

void LeeArchivos(EspacioMem em,char *entrada,int lineas,char *modo, char *salida,char *algoritmo){
	size_t ocupado, libre;

	if(strcmp(modo,"H")==0){
		printf("\nHistorial:\n");
	}else if(strcmp(modo,"S")==0){
		FILE *archivo=fopen(salida,"a");
		fprintf(archivo,"Historial:\n") ;
		fclose(archivo);
	}
	BloqueMem proc[lineas];
	int contador=0;
	FILE *archivo;
    size_t tamano;
    char nombre[NOMBRE_ARCHIVO], peticion[NOMBRE_PETICION], linea[LARGO_LINEA], *token;
    archivo = fopen(entrada,"r");
    while(feof(archivo) == 0){ 
        fgets(linea,LARGO_LINEA -1,archivo);                   
        token = strtok(linea,";");     strcpy(nombre,token);
        token = strtok(NULL,";");      strcpy(peticion,token);
        token = strtok(NULL,";\n");    tamano=(size_t)atoi(token);
        proc[contador]=crearProceso(nombre,tamano);

        if(strcmp(peticion,"allocate")==0){
        	if(strcmp(algoritmo,"F")==0){
        		agregarFirstFit(&em, &proc[contador],modo,salida);
        	}else if(strcmp(algoritmo,"N")==0){
        		agregarNextFit(&em, &proc[contador],modo,salida);
        	}else if(strcmp(algoritmo,"B")==0){
        		agregarBestFit(&em, &proc[contador],modo,salida);
        		//imprimirProcesosMemoria(&em);
        	}

        }else if(strcmp(peticion,"free")==0){
        	if(strcmp(modo,"H")==0){
				printf("Se elimina proceso ubicado en la dirección %zu\n",tamano);
			}else if(strcmp(modo,"S")==0){
				FILE *archivo=fopen(salida,"a");
				fprintf(archivo,"Se elimina proceso ubicado en la dirección %zu\n",tamano);
				fclose(archivo);
			}
        	eliminarProceso(&em,tamano);		// TODO eliminar MB2KB
        }
        
        contador++;
    }

    if (em.espacioOcupado >= em.tamanoTotal) {
    	ocupado = em.tamanoTotal;
    	libre = 0;
    }
    else {
    	ocupado = em.espacioOcupado;
    	libre = em.espacioLibre;	
    }
    
    //eliminarProceso(&em,5120);
    if(strcmp(modo,"H")==0){
    	printf("\nProcesos en memoria:\n");
    	imprimirMemoriaHablador(&em);
    	printf("\nEstadísticas de memoria:\nCantidad de desfragmentaciones: %zu\n", em.cantDefrags);
    	printf("Espacio ocupado: %zu\nEspacio libre: %zu\n", ocupado, libre);
    }else if(strcmp(modo,"S")==0){

    	FILE *archivo=fopen(salida,"a");
    	fprintf(archivo,"\nProcesos en memoria:\n"); 
    	fclose(archivo);
    	archivo=fopen(salida,"a");   	
    	imprimirMemoriaSilencioso(&em,salida);

    	fprintf(archivo,"\nEstadísticas de memoria:\nCantidad de desfragmentaciones: %zu\n", em.cantDefrags);
    	fprintf(archivo,"Espacio ocupado: %zu\nEspacio libre: %zu\n", ocupado, libre);
    	fclose(archivo);
    }
        
    fclose(archivo);
}

int cuentaLineas(char *entrada){
	FILE *archivo=fopen(entrada,"r");
	int contador=0;
	char linea[LARGO_LINEA];
	while(feof(archivo) == 0){ 
        fgets(linea,LARGO_LINEA -1,archivo);                   
        
        contador++;
    }

    fclose(archivo);
    return contador;
}




