/* Biblioteca para las Estructuras de Datos de Memoria */

#define TRUE 1
#define FALSE 0

#define STRING_BUFFER 128

typedef int boolean;	// Se define un alias "boolean" en base a enteros para diferenciarlos de éstos últimos

typedef struct BloqueMem_str {	// Bloque de Memoria
	char nombreProceso[STRING_BUFFER];
	size_t dirInicio;		// Usamos size_t en vez de int para evitar overflows con tamaños de memoria muy grandes
	size_t dirFin;
	size_t tamano;
	struct BloqueMem_str *sig;
} BloqueMem;


typedef struct EspacioMem_str {	// Espacio de Memoria (lista enlazada circular)
	size_t tamanoTotal;			// Tamaño total en KB del Espacio de Memoria
	size_t espacioOcupado;		// Cantidad de espacio ocupado en KB
	size_t espacioLibre;		// Cantidad de espacio libre en KB
	size_t cantDefrags;			// Contador de desfragmentaciones
	BloqueMem *inicio;			// Puntero al primer Bloque de Memoria
	BloqueMem *cursor;			// Puntero al ultimo Bloque de Memoria accedido
} EspacioMem;

typedef struct ListaProcesos{
	char procesos[100];
	struct ListaProcesos *sgte;
} LP;

typedef struct PilaProcesos_str {
	BloqueMem proceso;
	struct PilaProcesos_str *sig;
} PilaProcesos;

size_t MB2KB (size_t tamano) {		// convierte MB a KB
	return tamano*1024;
}

EspacioMem crearMemoria (size_t tamano) {
	EspacioMem em;

	em.tamanoTotal = MB2KB(tamano)-1;
	em.espacioOcupado = 0;
	em.espacioLibre = em.tamanoTotal;
	em.cantDefrags = 0;

	em.inicio = NULL;
	em.cursor = NULL;

	return em;
}

LP *creaLista(){
	LP *lp=(LP*)malloc(sizeof(LP));
	strcpy(lp->procesos,"");
	lp->sgte=NULL;
	return lp;
}

void insertar_lista(char* proceso,  LP *L){
	LP *temp;                                     
	temp = L->sgte;
	L->sgte = (LP *)malloc(sizeof(LP));     
	strcpy(L->sgte->procesos,proceso);
	L->sgte->sgte = temp; 
}

int buscarEnLista(char* proceso, LP *L){
	LP *aux = L;                      
	while(aux-> sgte != NULL){                  
		if(strcmp(aux->sgte->procesos,proceso) == 0)    
			return 0;
		aux = aux -> sgte;                      
	}
	return 1;
}

void imprimeListaHablador(char* proceso,BloqueMem *temp ){
	
	BloqueMem *recorre=temp;
	printf("%s:",proceso);
	while(recorre-> sig != NULL){                  
		if(strcmp(recorre->nombreProceso,proceso) == 0)    
			printf("%zu-%zu;",recorre->dirInicio,recorre->dirFin);
		
		recorre=recorre->sig;                    
	}
	if(strcmp(recorre->nombreProceso,proceso) == 0){    
			printf("%zu-%zu;\n",recorre->dirInicio,recorre->dirFin);
	}else{printf("\n");}
}

BloqueMem crearProceso(char* nombreProceso, size_t tamano) {
	BloqueMem bm;
	strcpy(bm.nombreProceso, nombreProceso);
	bm.dirInicio = 0;
	bm.dirFin = 0;
	bm.tamano = tamano;

	bm.sig = NULL;

	return bm;
}

void imprimirMemoriaHablador(EspacioMem *em) {
	BloqueMem *aux = (BloqueMem*)malloc(sizeof(BloqueMem));
	LP *lista=creaLista();
	aux = em->inicio;

	if (!aux) {
		printf("Memoria completamente vacia!\n");
	}
	else {
		insertar_lista(aux->nombreProceso,lista);
			imprimeListaHablador(aux->nombreProceso,aux);
			
		while (aux->sig) {
			aux = aux->sig;
			if(buscarEnLista(aux->nombreProceso,lista)==1){
				insertar_lista(aux->nombreProceso,lista);
				imprimeListaHablador(aux->nombreProceso,aux);
			}
		}
	}	
}

void imprimeErrorHablador(char *nombreProceso, size_t tamano){
	printf("%s;Se rechaza allocate por falta de memoria;%zu;\n",nombreProceso,tamano );
}

void imprimeListaSilencioso(char* proceso,BloqueMem *temp, char *salida ){
	FILE *archivo=fopen(salida,"a");
	BloqueMem *recorre=temp;
	fprintf(archivo,"%s:",proceso);
	while(recorre-> sig != NULL){                  
		if(strcmp(recorre->nombreProceso,proceso) == 0)    
			fprintf(archivo,"%zu-%zu;",recorre->dirInicio,recorre->dirFin);
		
		recorre=recorre->sig;                    
	}
	if(strcmp(recorre->nombreProceso,proceso) == 0){    
			fprintf(archivo,"%zu-%zu;\n",recorre->dirInicio,recorre->dirFin);
	}else{fprintf(archivo,"\n");}
	fclose(archivo);
}

void imprimirMemoriaSilencioso(EspacioMem *em, char *salida) {
	BloqueMem *aux = (BloqueMem*)malloc(sizeof(BloqueMem));
	LP *lista=creaLista();
	aux = em->inicio;

	if (!aux) {
		FILE *archivo=fopen(salida,"w");
		fprintf(archivo,"Memoria completamente vacia!\n");
		fclose(archivo);
	}
	else {
		insertar_lista(aux->nombreProceso,lista);
			imprimeListaSilencioso(aux->nombreProceso,aux,salida);
			
		while (aux->sig) {
			aux = aux->sig;
			if(buscarEnLista(aux->nombreProceso,lista)==1){
				insertar_lista(aux->nombreProceso,lista);
				imprimeListaSilencioso(aux->nombreProceso,aux,salida);
			}
		}
	}	
}

void imprimeErrorSilencioso(char *nombreProceso, size_t tamano,char *salida){
	FILE *archivo=fopen(salida,"a");
	fprintf(archivo,"%s;Se rechaza allocate por falta de memoria;%zu;\n",nombreProceso,tamano );
	fclose(archivo);
}